/* USES JQUERY */
/* global $ */

class XML {
    constructor(xml) 
    {
        if(xml instanceof XMLDocument)
            this.XML = [xml.children[0]];
        else if(xml instanceof Array && xml.length > 0)
            this.XML = xml;
        else
            this.XML = null;

        this.sortFunction = this.sortAsc;
    }

    sortAsc(a, b)
    {   
        if(a.innerHTML < b.innerHTML)
            return -1;
        else if(a.innerHTML > b.innerHTML)
            return 1;
        else
            return 0;
    }

    sortDesc(a, b)
    {
        if(a.innerHTML > b.innerHTML)
            return -1;
        else if(a.innerHTML < b.innerHTML)
            return 1;
        else
            return 0;
    }

    prepare(query)
    {
        if(query !== undefined && this.XML !== null)
            return new XML($(this.XML).find(query).toArray());

        return this;
    }

    get(query)
    {
        if(query !== undefined && this.XML !== null && this.XML.length > 0)
            return $(this.XML).find(query).toArray();

        return this.XML;
    }

    first(query)
    {
        if(this.XML === null || this.XML.length === 0)
            return null;

        if(query !== undefined)
        {
            var array = $(this.XML).find(query).toArray();
            return array.length > 0 ? array[0] : null;
        }

        return this.XML[0];
    }

    getDocument()
    {
        return this.XML;
    }

    deleteWhere(tag, values)
    {
        if( values === undefined || tag === undefined )
            return null;

        if( !(values instanceof Array) )
            values = [values];

        for(var i = 0; i < this.length(); i++)
        {
            for(var j = 0; j < values.length; j++)
            {
                if($(this.XML[i]).find(`${tag}:contains(${values[j]})`).length > 0)
                {
                    this.XML.splice(i, 1);
                    i--; // Countermeasure against i++ due to splice
                    break;
                }
            }
        }

        return this;
    }

    where(tag, values)
    {
        if( values === undefined || tag === undefined )
            return null;

        if( !(values instanceof Array) )
            values = [values];

        var search = [];

        for(var i = 0; i < this.length(); i++)
        {
            for(var j = 0; j < values.length; j++)
            {   
                if(i < this.length() && $(this.XML[i]).find(`${tag}:contains(${values[j]})`).length > 0)
                {
                    search.push(this.XML[i]);
                    break;
                }
            }
        }
        return new XML( search );
    }

    sortBy(tags, orders)
    {
        if(this.XML === null)
            return null;

        if( tags === undefined || orders === undefined )
            return this;

        if( !(tags instanceof Array) )
            tags = [tags];

        if( !(orders instanceof Array) )
            orders = [orders];

        if(orders.length < tags.length)
        {
            let lastOrder = orders[orders.length - 1];
            while(orders.length < tags.length)
                orders.push(lastOrder);
        }

        if(tags.length < orders.length)
        {
            let lastTag = tags[tags.length - 1];
            while(tags.length < orders.length)
                tags.push(lastTag);
        }

        var actualOrder = 0;
        var actualTag = 0;

        return new XML( this.sort(tags, actualTag, orders, actualOrder, this.XML) );
    }

    sort(tags, tagIndex, orders, orderIndex, array)
    {
        if(tagIndex >= tags.length)
            return array;
        else
        {
            var f;
            switch(orders[orderIndex])
            {
                case 'asc':
                default:
                    f = this.sorcAsc;
                break;

                case 'desc':
                    f = this.sortDesc;
                break;
            }

            var sorted = $(array).find(tags[tagIndex]).toArray().sort(f);
            var sortedDocument = [];
            var sortedPartial = [];

            while(sorted.length > 0)
            {
                sortedPartial = [sorted.shift()];
                while(sorted.length > 0 && sortedPartial[0].innerHTML == sorted[0].innerHTML)
                    sortedPartial.push(sorted.shift());

                sortedPartial = this.sort(tags, tagIndex + 1, orders, orderIndex + 1, $(sortedPartial).closest(this.XML[0].nodeName).toArray() );

                for(var i = 0; i < sortedPartial.length; i++)
                    sortedDocument.push( $(sortedPartial[i]).closest(this.XML[0].nodeName).toArray()[0] );

            }

            return sortedDocument;
        }
    }

    length()
    {
        return this.XML !== null ? this.XML.length : 0;
    }

    filterToOne(tag, value)
    {
        if(this.XML === null)
            return null;

        if( typeof value !== 'string' )
            value = '';

        for(var i = 0; i < this.length(); i++)
        {
            var toFilter = $(this.XML[i]).find(`${tag}:contains(${value})`);

            while(toFilter.length === 0 && i < this.length())
                toFilter = $(this.XML[++i]).find(`${tag}:contains(${value})`);

            if(toFilter.length > 0)
                toFilter = toFilter.html();

            for(var j = i + 1; j < this.length(); )
            {
                var search = $(this.XML[j]).find(`${tag}:contains(${value})`);
                if(search.length > 0 && search.html() === toFilter)
                    this.XML.splice(j, 1);
                else
                    j++;
            }
        }

        return this;
    }
}

class Dataframe {
    constructor(data_path, wait)
    {   
        Dataframe.load(data_path, this, wait);
    }

    getXML(query)
    {
        return this.document !== undefined ? this.document.prepare(query) : null;
    }

    static load(data_path, dataframe, wait)
    {
        dataframe.document = null;

        $.ajax({
            url: data_path,

            type: "POST",

            dataType: "xml",

            data: {},

            success: function(response) {
                dataframe.document = new XML(response);
            },

            error: function() {
                console.error(`ERROR: Could not load dataframe from path "${path}"`);
            }
        });

        if( (wait === true) || ('wait'.localeCompare(wait) === 1) )
        {
            var wait_load = function() {
                if(dataframe.document === null)
                    setTimeout(wait_load, 800);
            }
            wait_load();
        }
    }
}
