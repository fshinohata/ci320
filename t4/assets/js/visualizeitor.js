/* USES JQUERY */
/* global $ */

/* USES dataframe_manager.js */
/* global DataFrame */

/* ############################## */
/* CLASSES                        */
/* ############################## */

class Visualizeitor {
    constructor(data_path, wait)
    {
        this.cellStyler = {
            "Reprovado por nota":                  { state: 'reproved' },
            "Reprovado sem nota":                  { state: 'reproved' },
            "Reprovado por Frequência":            { state: 'reproved' },
            "Aprovado":                            { state: 'approved' },
            "Dispensa de Disciplinas (com nota)":  { state: 'approved' },
            "Equivalência de Disciplina":          { state: 'equivalence' },
            "Matrícula":                           { state: 'enrolled' },
            "Trancamento Administrativo":          { state: 'cancelled' },
            "Trancamento Total":                   { state: 'cancelled' },
            "Cancelado":                           { state: 'cancelled' }
        };

        this.dataframe = new Dataframe(data_path, wait);
        this.html_tree = null;
    }

    cellStyling(situation)
    {
        return this.cellStyler[situation];
    }

    getXML(query)
    {
        return this.dataframe.getXML(query);
    }
}

class DOMGenerator {
    static error(target, message)
    {
        var error = document.createElement("P");
        var target_pos = $(target).offset();
        error.classList.add('error-message');
        error.style.position = "absolute";
        error.style.top = `${target_pos.top + $(target).outerHeight()}px`;
        error.style.left = `${target_pos.left}px`;
        error.innerHTML = message;
        
        return error;
    }

    static modal(title, content)
    {
        if( title instanceof HTMLElement )
            title = title.outerHTML;

        if( content instanceof HTMLElement )
            content = content.outerHTML;

        if( typeof title !== "string" || typeof content !== "string" )
            return;

        var darken = document.createElement("DIV");
        darken.classList.add('darken');
        darken.classList.add('fullscreen-fixed');
        document.body.appendChild(darken);

        var modal_container = document.createElement("DIV");
        modal_container.classList.add('modal-container');
        modal_container.classList.add('fullscreen-fixed');
        modal_container.classList.add('flex');
        modal_container.classList.add('flex-ai-c');
        modal_container.classList.add('flex-jc-c');

        var modal = document.createElement("DIV");
        modal.classList.add('modal');

        var modal_title = document.createElement("DIV");
        modal_title.classList.add('modal__title');
        modal_title.innerHTML = title;
        modal.appendChild(modal_title);

        var modal_content = document.createElement("DIV");
        modal_content.classList.add('modal__content');
        modal_content.innerHTML = content;
        modal.appendChild(modal_content);
        
        modal_container.appendChild(modal);
        document.body.appendChild(modal_container);
        modal.focus();
        modal_container.addEventListener('click', function(e) {
            if( e.target == modal_container )
            {
                document.body.removeChild(modal_container);
                document.body.removeChild(darken);
            }
        });
    }

    static table(options, data)
    {
        var tableDOM = document.createElement("TABLE");

        if( options.id !== undefined && typeof options.id === 'string' )
            tableDOM.id = options.id;

        if( options.class !== undefined )
        {
            if( !(options.class instanceof Array) )
                options.class = [options.class];

            for(var i = 0; i < options.class.length; i++)
                tableDOM.classList.add(options.class[i]);
        }

        if( options.extra_attr !== undefined && options.extra_attr instanceof Object)
        {
            for(var attr of Object.getOwnPropertyNames(options.extra_attr))
                tableDOM.setAttribute(attr, options.extra_attr[attr]);
        }

        if( options.header !== undefined && (options.header instanceof Array) )
        {
            var theader = document.createElement("THEAD");
            var headerRow = document.createElement("TR");

            for(var i = 0; i < options.header.length; i++)
                headerRow.innerHTML += `<th>${options.header[i]}</th>`;

            theader.appendChild(headerRow);
            tableDOM.appendChild(theader);
        }
        
        if( data !== undefined && (data instanceof Array) )
        {
            var tbody = document.createElement("TBODY");

            for(var i = 0; i < data.length; i++)
                if( !(data[i] instanceof Array) )
                    data[i] = [data[i]];

            var limit = data[0].length;

            for(var i = 0; i < data.length; i++)
                if( data[i].length > limit )
                    limit = data[i].length;


            for(var i = 0; i < limit; i++)
            {
                var bodyRow = document.createElement("TR");
                
                for(var j = 0; j < data.length; j++)
                {
                    bodyRow.innerHTML += `<td>${ data[j][i] !== undefined ? data[j][i] : '' }</td>`;
                }

                tbody.appendChild(bodyRow);
            }

            tableDOM.appendChild(tbody);
        }

        return tableDOM;
    }
}



/* ############################## */
/* GLOBAL VARIABLES               */
/* ############################## */

var DATA_PATH = "data/alunos.xml";
var v = null;
var last_search = null;


/* ############################## */
/* FUNCTIONS                      */
/* ############################## */

$(document).ready(function() {
    v = new Visualizeitor(DATA_PATH, 'WAIT');

    main();
});

// Helper function for $.map()
function getHTML()
{
    return this.innerHTML;
}

function main()
{
    // Shows search field
    $("#pesquisa").load("_pesquisa.html", function() {
        var $search_input = $("#pesquisa input[type=text]");
        var $search_form = $("#pesquisa #search-form");
        var validator = /^[0-9]{8}$/;

        $search_input.on('input', function() {
            $("#pesquisa .error-message").remove();

            if(this.value.length == 0)
            {
                $(this).css("border", "");
            }
            else if(validator.test(this.value) === true)
            {
                $(this).css("border", "2px solid green");
            }
            else
            {
                $(this).css("border", "2px solid red");
            }
        });



        /*
         * SEARCH FOR A GIVEN GRR
         */
        $search_form.submit(function(e) {
            e.preventDefault();

            // If GRR is valid and is not shown already
            if(validator.test($search_input.val()) === true && $search_input.val() !== last_search)
            {
                last_search = $search_input.val();
                var data = v.getXML(`ALUNO:contains(GRR${$search_input.val()})`);

                // If there is data for the given GRR, loads and process the data
                if( data.first() !== null)
                {
                    mostRecentData = data.sortBy(['ANO', 'PERIODO'], 'desc').filterToOne('COD_ATIV_CURRIC').filterToOne('DESCR_ESTRUTURA', 'Trabalho de Graduação I').filterToOne('DESCR_ESTRUTURA', 'Trabalho de Graduação II');
                    
                    // Reloads table and restart check
                    $("#grade-curricular").load('_grade-bcc.html', function() { 
                        data_processor(mostRecentData);

                        /*
                         * RELOAD EVENT LISTENERS
                         */
                        $("#grade-curricular > table > tbody > tr > td").on('click', function(e) {
                            if(this.dataset.code !== undefined)
                            {
                                var lastEntry = data.sortBy(['ANO', 'PERIODO'], 'desc').filterToOne('COD_ATIV_CURRIC').where('COD_ATIV_CURRIC', this.dataset.code);
                                var tableData = [ 
                                    lastEntry.first('COD_ATIV_CURRIC').innerHTML + " -- " + lastEntry.first('NOME_ATIV_CURRIC').innerHTML,
                                    lastEntry.first('PERIODO').innerHTML + " de " + lastEntry.first('ANO').innerHTML,
                                    lastEntry.first('MEDIA_FINAL').innerHTML,
                                    lastEntry.first('FREQUENCIA').innerHTML
                                ];
                                var table = DOMGenerator.table({
                                    header: ['Código/Nome da Disciplina', 'Última vez cursada', 'Nota', 'Frequência'],
                                    class: ['beautify-table'],
                                }, tableData);

                                DOMGenerator.modal(lastEntry.first('NOME_ALUNO').innerHTML, table);
                            }
                            else
                                DOMGenerator.modal('ERRO', 'Não há registros desta disciplina para este aluno.');
                        });

                        $("#grade-curricular > table > tbody > tr > td").on('contextmenu', function(e) {
                            e.preventDefault();
                            
                            if(this.dataset.code !== undefined)
                            {
                                var entries = data.sortBy(['ANO', 'PERIODO'], 'desc').where('COD_ATIV_CURRIC', this.dataset.code);
                                var disciplines = [];

                                var codes = entries.get('COD_ATIV_CURRIC');
                                var names = entries.get('NOME_ATIV_CURRIC');

                                for(var i = 0; i < codes.length; i++)
                                    disciplines.push(codes[i].innerHTML + " -- " + names[i].innerHTML);

                                var dates = [];
                                var years = entries.get('ANO');
                                var semesters = entries.get('PERIODO');

                                for(var i = 0; i < years.length; i++)
                                    dates.push(semesters[i].innerHTML + " de " + years[i].innerHTML);

                                var tableData = [ 
                                    disciplines,
                                    dates,
                                    $(entries.get('MEDIA_FINAL')).map(getHTML).get(),
                                    $(entries.get('FREQUENCIA')).map(getHTML).get()
                                ];
                                var table = DOMGenerator.table({
                                    header: ['Código/Nome da Disciplina', 'Período', 'Nota', 'Frequência'],
                                    class: ['beautify-table'],
                                }, tableData);

                                DOMGenerator.modal(entries.first('NOME_ALUNO').innerHTML, table);
                            }

                            return false;
                        });
                    });

                }
                else // Or just reload the table
                {
                    if( $("#pesquisa").find('.error-message').length === 0 )
                    {   
                        var error = DOMGenerator.error($search_input, "ERRO: Não há dados para este GRR!");
                        $("#pesquisa").append(error);
                    }
                    $("#grade-curricular").load('_grade-bcc.html');
                }

            }
            else if( validator.test($search_input.val()) === false )
            {
                // If the input is invalid, generate a error DOM
                if( $("#pesquisa").find('.error-message').length === 0 )
                {
                    var error = DOMGenerator.error($search_input, "ERRO: GRR inválido!");
                    $("#pesquisa").append(error);
                }
                $("#grade-curricular").load('_grade-bcc.html');
            }
        });
    });

    // Shows disciplines
    $("#grade-curricular").show();
    $("#grade-curricular").load('_grade-bcc.html');


    // Test table for analysis
    // $("#teste").show();
    // $("#teste").html(v.toHTML());

}

function cell_processor(cell, data, delete_data)
{
    var entry;

    if( delete_data === undefined )
        delete_data = true;

    switch(cell.innerHTML)
    {
        default:
            entry = data.where('COD_ATIV_CURRIC', cell.innerHTML);

            if(entry.first() === null)
                entry = data.where("NOME_ATIV_CURRIC", cell.innerHTML);

        break;

        case 'OPT':
            var optatives = [
                'Optativas',
                'Disciplinas de outros cursos',
                'Discipl. de outros currículos do curso',
            ];

            entry = data.where('DESCR_ESTRUTURA', optatives);

            if( entry.first('COD_ATIV_CURRIC') !== null )
                cell.innerHTML = entry.first('COD_ATIV_CURRIC').innerHTML;
        break;

        case 'TG I':
            entry = data.where('DESCR_ESTRUTURA', 'Trabalho de Graduação I');
        break;

        case 'TG II':
            entry = data.where('DESCR_ESTRUTURA', 'Trabalho de Graduação II');
        break;
    }

    if( entry.first('COD_ATIV_CURRIC') !== null )
    {
        var cellStyle = v.cellStyling(entry.first('SITUACAO').innerHTML);
        cell.dataset.state = cellStyle.state;
        cell.dataset.code = entry.first('COD_ATIV_CURRIC').innerHTML;

        if(delete_data)
            data = data.deleteWhere('COD_ATIV_CURRIC', entry.first('COD_ATIV_CURRIC').innerHTML);
    }
}

function data_processor(data) 
{


    /*
     * DATA PROCESSING AND CELL STYLING
     */
    $("#grade-curricular > table > tbody > tr > td").each(function() { cell_processor(this, data); });


    /*
     * Generation of table with remaining disciplines
     */

    if( data.first() !== null )
    {
        console.log(data.first())
        // Table generation
        var remaining = DOMGenerator.table({ 
            id: 'remaining-disciplines',
            header: ['Código', 'Disciplina'], 
            class: ['beautify-table', 'clickable-cells'],
            extra_attr: { oncontextmenu: "return false;" }
        }, [ $(data.get('COD_ATIV_CURRIC')).map(getHTML).get(), $(data.get('NOME_ATIV_CURRIC')).map(getHTML).get() ]);
        
        // Container Title and substitle
        var title = document.createElement("H1");
        title.classList.add('container__title');
        title.innerHTML = "Disciplinas Remanescentes";
        
        var subtitle = document.createElement("H2");
        subtitle.classList.add('container__subtitle');
        subtitle.innerHTML = "Lista de disciplinas que não estão contempladas na grade atual, ou optativas extras além das 6 requeridas";

        // Append of generated elements
        $("#grade-curricular").append(title);
        $("#grade-curricular").append(subtitle);
        $("#grade-curricular").append(remaining);
        
        $("#grade-curricular #remaining-disciplines > tbody > tr > td").each(function() { cell_processor(this, data, false); });
    }
    
}