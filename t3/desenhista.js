/*
 * CONSTANTS
 */

var MOUSE_LEFT_CLICK = 0;
var MOUSE_RIGHT_CLICK = 2;
var LINE_SELECTION_MAXIMUM_DISTANCE = 30;
var LINE_PART_A = 'a';
var LINE_PART_MID = 'mid';
var LINE_PART_B = 'b';
var CANVAS_DEFAULT_COLOR = 'rgb(0,0,0)';



/*
 * OBJECTS
 */

class Canvas {
    /* Canvas constructor(string canvas_id) */
    constructor(canvas_id)
    {
        this.lines = [];
        this.canvas = document.getElementById(canvas_id);
        this.pencil = this.canvas.getContext('2d');
        this.set_color(CANVAS_DEFAULT_COLOR);
    }

    /* void set_color(string color) */
    set_color(color)
    {
        this.pencil.fillStyle = color;
    }

    /* void set_size(int width, int height) */
    set_size(width, height)
    {
        this.canvas.width = width;
        this.canvas.height = height;
    }

    /* void add_line(Point a, Point b) */
    add_line(a, b)
    {
        var v = new Point(a.x, a.y);
        var u = new Point(b.x, b.y);
        this.lines.push(new Line(v, u));
    }

    /* void remove_line(Line line) */
    remove_line(line)
    {
        var index = this.lines.indexOf(line);

        if( index >= 0 )
            this.lines.splice(index, 1);
    }

    /* void reset_lines(void) */
    reset_lines()
    {
        this.lines = [];
    }

    /* void clear(void) */
    clear()
    {
        this.pencil.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    /* void reset(void) */
    reset()
    {
        this.clear();
        this.reset_lines();
    }

    /* Line nearest_line(Point p) */
    nearest_line(p)
    {
        if(this.lines.length > 0)
        {   
            var nearest = this.lines[0];
            for(var line of this.lines)
            {
                var dist = Point.distance(line.nearest_point(p), p);
                if(dist < Point.distance(nearest.nearest_point(p), p))
                    nearest = line;
            }

            return nearest;
        }

        return null;
    }
}

class Line {
    /* Line constructor(Point a, Point b) */
    constructor(a, b)
    {
        this.a = a;
        this.b = b;
        this.mid = Point.mid(a, b);
    }

    /* Point nearest_point(Point p) */
    nearest_point(p)
    {
        // Calculates vector v of line
        var v = new Point(this.b.x - this.a.x, this.b.y - this.a.y);
        var intersection;

        if(v.x != 0.0)
        {            
            var vm = v.y / v.x; // Vector angle

            if(vm != 0.0)
            {
                // Calculates intersection of the line
                // described by the orthogonal vector of v
                var um = -(1 / vm);
                var ux = ( (p.y - this.b.y) + (vm * this.b.x - um * p.x) ) / ( vm - um );
                var uy = p.y + um * ux - um * p.x;
                intersection = new Point(ux, uy);
            }
            else
            {
                // If vm == 0.0, then the line is horizontal
                intersection = new Point(p.x, this.a.y);
            }
        }
        else
        {
            intersection = new Point(this.a.x, p.y);
        }

        var min = new Point( this.a.x < this.b.x ? this.a.x : this.b.x ,
                             this.a.y < this.b.y ? this.a.y : this.b.y );

        var max = new Point( this.a.x > this.b.x ? this.a.x : this.b.x ,
                             this.a.y > this.b.y ? this.a.y : this.b.y );

        if( min.x <= intersection.x && intersection.x <= max.x )
            return intersection;
        else if(Point.distance(this.a, p) < Point.distance(this.b, p))
            return this.a;
        else
            return this.b;
    }
}

class Point {
    /* Point constructor(int x, int y) */
    constructor(x, y)
    {
        this.x = x;
        this.y = y;
    }

    /* Point mid(Point a, Point b) */
    static mid(a, b)
    {
        return new Point( (a.x + b.x)/2 , (a.y + b.y)/2 );
    }

    /* float distance(Point a, Point b) */
    static distance(a, b)
    {
        var v = b.x - a.x;
        v = v * v;

        var u = b.y - a.y;
        u = u * u;

        return Math.sqrt(v + u);
    }

    /* float module(Point p) */
    static module(p)
    {
        return Point.distance(p, new Point(0,0));
    }

    static multiply(v, u)
    {
        return (v.x * u.x + v.y * u.y);
    }
}

class Draftsman {
    /* Draftsman constructor(Canvas canvas_object) */
    constructor(canvas_object)
    {
        this.c = canvas_object;
    }

    /* void create_line(Point from, Point to) */
    create_line(from, to)
    {
        this.c.add_line(from, to);
        this.stroke(from, to);
    }

    /* void stroke(Point from, Point to) */
    stroke(from, to)
    {
        this.c.pencil.beginPath();
        this.c.pencil.moveTo(from.x, from.y);
        this.c.pencil.lineTo(to.x, to.y);
        this.c.pencil.stroke();
    }

    /* void polygon(Point center, int radius, int sides) */
    polygon(center, radius, sides)
    {
        var p = new Point(center.x + radius, center.y);

        // Actual coordinates

        var arc = 2 * Math.PI;
        var slice = arc / sides;

        for(var i = 1; i <= sides; i++)
        {
            var angle = slice * i;
            var tmp = new Point(p.x, p.y); // Actual point p

            // Updates p
            p.x = center.x + radius * Math.cos(angle);
            p.y = center.y + radius * Math.sin(angle);

            this.create_line(tmp, p);
        }
    }

    /* void redraw(void) */
    redraw()
    {
        this.c.clear();
        
        for(var line of this.c.lines)
        {
            this.stroke(line.a, line.b);
        }
    }
}



/*
 * GLOBAL VARIABLES
 */

var c = new Canvas('the-canvas');
var mouse_pos_logger = document.getElementById('mouse-coordinates');
var mouse_left_button_is_down = false;
var selected_line = null;
var selected_line_part = null;


/*
 * FUNCTIONS
 */

/* void main(void) */
function main()
{
    var slider_input = document.getElementById('slider-input');
    var slider_button = document.getElementById('go');
    var d = new Draftsman(c);
    var center = new Point(window.innerWidth / 2, window.innerHeight / 2);

    c.set_size(window.innerWidth, window.innerHeight);
    c.set_color('rgb(0,0,0)');

    d.create_line(new Point(center.x, center.y - 150), new Point(center.x, center.y + 150));

    slider_input.addEventListener('input', function(e) {
        document.getElementById('slider-value').innerHTML = this.value;
    });

    slider_button.addEventListener('click', function(e) {
        // Clear canvas and redraw new polygon
        c.reset();
        d.polygon(center, 300, slider_input.value);
    });

    c.canvas.addEventListener('mousedown', function(e) {
        var mousepos = new Point(e.clientX, e.clientY);
        selected_line = c.nearest_line(mousepos);
        var nearest_point = selected_line.nearest_point(mousepos);
        
        // Marks the left click as pressed
        if(e.button == MOUSE_LEFT_CLICK)
        {
            mouse_left_button_is_down = true;

            if( Point.distance(nearest_point, mousepos) <= LINE_SELECTION_MAXIMUM_DISTANCE)
            {
                var nearest_part = selected_line.a
                selected_line_part = LINE_PART_A;
                
                if( Point.distance(selected_line.mid, mousepos) < Point.distance(nearest_part, mousepos) )
                {
                    nearest_part = selected_line.mid;
                    selected_line_part = LINE_PART_MID;
                }

                if( Point.distance(selected_line.b, mousepos) < Point.distance(nearest_part, mousepos) )
                {
                    nearest_part = selected_line.b;
                    selected_line_part = LINE_PART_B;
                }
            }
        }
        else if(e.button == MOUSE_RIGHT_CLICK)
        {
            if( (nearest_point != selected_line.a) && (nearest_point != selected_line.b) )
            {   
                if(Point.distance(nearest_point, mousepos) <= LINE_SELECTION_MAXIMUM_DISTANCE)
                {
                    c.remove_line(selected_line);

                    c.add_line(selected_line.a, nearest_point);
                    c.add_line(selected_line.b, nearest_point);
                }
            }
        }
    });

    c.canvas.addEventListener('mouseup', function(e) {
        // Marks the left click as unpressed
        if(e.button == MOUSE_LEFT_CLICK)
        {
            mouse_left_button_is_down = false;
            selected_line = null;
            selected_line_part = null;
        }
    });

    c.canvas.addEventListener('mousemove', function(e) {
        if(mouse_left_button_is_down && selected_line)
        {
            var mousepos = new Point(e.clientX, e.clientY);

            switch(selected_line_part)
            {
                case LINE_PART_A:
                    selected_line.a = mousepos;
                    selected_line.mid = Point.mid(selected_line.a, selected_line.b);
                break;

                case LINE_PART_MID:
                    var diff = new Point(mousepos.x - selected_line.mid.x, mousepos.y - selected_line.mid.y);
                    selected_line.a.x += diff.x;
                    selected_line.a.y += diff.y;
                    selected_line.b.x += diff.x;
                    selected_line.b.y += diff.y;
                    selected_line.mid.x += diff.x;
                    selected_line.mid.y += diff.y;
                break;

                case LINE_PART_B:
                    selected_line.b = mousepos;
                    selected_line.mid = Point.mid(selected_line.a, selected_line.b);
                break;
            }
            
            d.redraw();
        }
        mouse_pos_logger.innerHTML = `X: ${e.clientX}\tY: ${e.clientY}`;
    });
}
