# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w( home.css home.js )
Rails.application.config.assets.precompile += %w( tasks.css tasks.js )
Rails.application.config.assets.precompile += %w( due_dates.css due_dates.js )
Rails.application.config.assets.precompile += %w( task_states.css task_states.js )
Rails.application.config.assets.precompile += %w( taskboards.css taskboards.js )
Rails.application.config.assets.precompile += %w( taskboard_groups.css taskboard_groups.js )
Rails.application.config.assets.precompile += %w( devise/sessions.css )
Rails.application.config.assets.precompile += %w( jkanban/jkanban.min.css jkanban/jkanban.min.js )
Rails.application.config.assets.precompile += %w( jquery.min.js )
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
