Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users
  
  # Route :new for tasks must be slightly different
  resources :tasks, except: [:index, :new]
  get 'task/new/:id', to: 'tasks#new', as: 'new-task'

  # Taskboard routes
  resources :taskboards
  post 'taskboards/load/:group', to: 'taskboards#indexLoadTaskboards', as: 'taskboards-load-group'
  get 'dismiss', to: 'taskboards#dismissModal', as: 'taskboards-dismiss'
  
  resources :taskboard_groups, except: [:index, :show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
