json.extract! taskboard_group, :id, :name, :created_at, :updated_at
json.url taskboard_group_url(taskboard_group, format: :json)
