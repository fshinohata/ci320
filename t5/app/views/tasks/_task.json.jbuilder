json.extract! task, :id, :name, :description, :task_state_id, :due_date_id, :taskboard_id, :created_at, :updated_at
json.url task_url(task, format: :json)
