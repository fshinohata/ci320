class TaskboardGroup < ApplicationRecord
    has_many :taskboard_groups_taskboards
    has_many :taskboards, through: :taskboard_groups_taskboards do
        def << (taskboard)
            begin
                super taskboard
                return true
            rescue => exception
                return false
            end
        end
    end
    
    validates_presence_of :name

    def save!
        begin
            super
            return true
        rescue => exception
            return false
        end
    end
end
