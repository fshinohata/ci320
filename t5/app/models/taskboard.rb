class Taskboard < ApplicationRecord
    has_many :tasks, dependent: :destroy
    has_many :taskboard_groups_taskboards
    has_many :taskboard_groups, through: :taskboard_groups_taskboards do
        def << (taskboardGroup)
            begin
                super taskboardGroup
                return true
            rescue => exception
                return false
            end
        end
    end
    
    validates_presence_of :name

    def save!
        begin
            super
            return true
        rescue => exception
            return false
        end
    end
end
