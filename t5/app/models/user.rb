class User < ApplicationRecord
  has_many :roles_users
  has_many :roles, through: :roles_users do
    def << (role)
      begin
        super role
        return true
      rescue => exception
        return false
      end
    end
  end

  validates_uniqueness_of :email

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :validatable, :rememberable
         #,:registerable, :recoverable, :trackable

  def save!
    begin
      super
      return true
    rescue => exception
      return false
    end
  end

  def isAdmin
    return self.roles.where(name: :administrator).exists?
  end
end
