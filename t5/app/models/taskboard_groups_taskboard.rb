class TaskboardGroupsTaskboard < ApplicationRecord
  belongs_to :taskboard
  belongs_to :taskboard_group
  validates_uniqueness_of :taskboard_id, scope: :taskboard_group_id 
end
