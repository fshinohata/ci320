class Role < ApplicationRecord
    has_many :roles_users
    has_many :users, through: :roles_users, dependent: :destroy do
        def << (user)
            begin
                super user
                return true
            rescue => exception
                return false
            end
        end
    end

    def save!
    begin
      super
      return true
    rescue => exception
      return false
    end
  end
    
    validates_uniqueness_of :name
end
