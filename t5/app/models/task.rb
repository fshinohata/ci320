class Task < ApplicationRecord
  belongs_to :task_state
  belongs_to :due_date, dependent: :destroy
  belongs_to :taskboard
  
  validates_presence_of :name
end
