# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
class TaskboardManager
    constructor: ->
      @modal = document.getElementById "form-modal"
      @formContainer = document.getElementById "form-container"
      @modal.parentNode.removeChild @modal
      @modal.style.display = 'block'
    
    showModal: ->
      document.body.appendChild @modal
    
    hideModal: ->
      @modal.classList.add 'animation-disappear'
      r = @
      setTimeout ( ->
        document.body.removeChild r.modal
        r.modal.classList.remove 'animation-disappear'
      ), 300
    
    setForm: (html) ->
      @formContainer.innerHTML = html

root = exports ? this

$(document).ready(() ->
  root.taskboardManager = new TaskboardManager
)