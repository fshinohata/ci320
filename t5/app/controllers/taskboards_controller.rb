class TaskboardsController < ApplicationController
  before_action :set_taskboard, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /taskboards
  # GET /taskboards.json
  def index
    @pageTitle = 'View Taskboards'
    @navbarTitle = 'Manage your taskboards'
    @mainTitle = 'Taskboards'

    @taskboards = Taskboard.all
    @task = Task.new
    @taskStates = TaskState.all
    
    @taskboardGroups = TaskboardGroup.all
  end

  # POST /taskboards/load
  # POST /taskboards/load.json
  def indexLoadTaskboards
    @taskboardGroupId = taskboard_group_params[:group]

    if @taskboardGroupId == '0'
      @taskboards = Taskboard.all
    else
      @taskboards = TaskboardGroup.find(@taskboardGroupId).taskboards.all
    end

    @action = 'show-taskboard-group'

    respond_to do |format|
      format.html { render :index }
      format.js { render :index }
    end
  end

  # GET /taskboards/dismiss
  # GET /taskboards/dismiss.json
  def dismissModal
    @action = 'dismiss-modal'
    respond_to do |format|
      format.js { render :index }
    end
  end
  
  

  # GET /taskboards/1
  # GET /taskboards/1.json
  def show
    @action = 'show-taskboard'
    
    respond_to do |format|
      format.js { render :index }
    end
  end

  # GET /taskboards/new
  def new
    @taskboard = Taskboard.new
    @taskboard_groups = TaskboardGroup.all
    @action = 'new-taskboard'

    respond_to do |format|
      format.js { render :index }
    end
  end

  # GET /taskboards/1/edit
  def edit
    @taskboard_groups = TaskboardGroup.all
    @action = 'edit-taskboard'

    respond_to do |format|
      format.js { render :index }
    end
  end

  # POST /taskboards
  # POST /taskboards.json
  def create
    @taskboard = Taskboard.new(taskboard_params)
    
    taskboard_groups_params.each { |group| 
      # First element of collection_select is always "" for some reason
      if group != ""
        @taskboard.taskboard_groups << TaskboardGroup.find_by(id: group)
      end
    }

    respond_to do |format|
      if @taskboard.save
        format.html { redirect_to taskboards_path, notice: "Taskboard '#{@taskboard.name}' was successfully created." }
      else
        format.html { redirect_to taskboards_path, alert: "Taskboard '#{@taskboard.name}' could not be created." }
      end
    end
  end

  # PATCH/PUT /taskboards/1
  # PATCH/PUT /taskboards/1.json
  def update
    oldName = @taskboard.name
    @taskboard.name = taskboard_params[:name]

    if taskboard_groups_params.length > 1
      @taskboard.taskboard_groups.delete_all

      taskboard_groups_params.each { |group| 
        # First element of collection_select is always "" for some reason
        if group != ""
          @taskboard.taskboard_groups << TaskboardGroup.find_by(id: group)
        end
      }
    end

    respond_to do |format|
      if @taskboard.save
        format.html { redirect_to taskboards_path, notice: "Taskboard '#{oldName}' was successfully updated." }
      else
        format.html { redirect_to taskboards_path, alert: "Taskboard '#{oldName}' could not be updated." }
      end
    end
  end

  # DELETE /taskboards/1
  # DELETE /taskboards/1.json
  def destroy
    @taskboard.destroy
    respond_to do |format|
      format.html { redirect_to taskboards_url, notice: 'Taskboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_taskboard
      @taskboard = Taskboard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def taskboard_params
      params.require(:taskboard).permit(:name, :taskboard_group)
    end

    def taskboard_groups_params
      params.require(:taskboard_group).require(:id)
    end

    def taskboard_group_params
      params.permit(:group)
    end
end
