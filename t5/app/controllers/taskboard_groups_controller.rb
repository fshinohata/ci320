class TaskboardGroupsController < ApplicationController
  before_action :set_taskboard_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /taskboard_groups/new
  def new
    @taskboard_group = TaskboardGroup.new
    @action = 'new-taskboard-group'

    respond_to do |format|
      format.js { render '/taskboards/index' }
    end
  end
  
  # GET /taskboard_groups/1/edit
  def edit
    @action = 'edit-taskboard-group'
    respond_to do |format|
      format.js { render 'taskboards/index' }
    end
  end

  # POST /taskboard_groups
  # POST /taskboard_groups.json
  def create
    @taskboard_group = TaskboardGroup.new(taskboard_group_params)

    respond_to do |format|
      if @taskboard_group.save
        format.html { redirect_to taskboards_path, notice: 'Taskboard group was successfully created.' }
      else
        format.html { redirect_to taskboards_path, alert: 'Taskboard group could not be created.' }
      end
    end
  end

  # PATCH/PUT /taskboard_groups/1
  # PATCH/PUT /taskboard_groups/1.json
  def update
    oldName = @taskboard_group.name

    respond_to do |format|
      if @taskboard_group.update(taskboard_group_params)
        format.html { redirect_to taskboards_path, notice: "Taskboard group '#{oldName}' was successfully updated." }
      else
        format.html { redirect_to taskboards_path, alert: "Could not update taskboard group '#{oldName}'." }
      end
    end
  end

  # DELETE /taskboard_groups/1
  # DELETE /taskboard_groups/1.json
  def destroy
    @taskboard_group.destroy
    respond_to do |format|
      format.html { redirect_to taskboards_path, notice: 'Taskboard group was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_taskboard_group
      @taskboard_group = TaskboardGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def taskboard_group_params
      params.require(:taskboard_group).permit(:name)
    end
end
