require 'time'

class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @action = 'show-task'

    respond_to do |format|
      format.js { render 'taskboards/index' }
    end
  end

  # GET /tasks/new
  def new
    @task = Task.new
    @taskStates = TaskState.all
    @taskboardId = params[:id]
    @action = 'new-task'

    respond_to do |format|
      format.js { render 'taskboards/index' }
    end
  end

  # GET /tasks/1/edit
  def edit
    @action = 'edit-task'
    @taskboardId = @task.taskboard.id
    @taskStates = TaskState.all

    respond_to do |format|
      format.js { render '/taskboards/index' }
    end
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @taskboard = Taskboard.find_by(id: task_params[:taskboard_id])
    d = params[:due_date]
    @dueDate = DueDate.create date: "#{d['date(1i)']}-#{d['date(2i)']}-#{d['date(3i)']} #{d['date(4i)']}:#{d['date(5i)']}:00"
    @task = Task.create name: task_params[:name],
                        description: task_params[:description],
                        task_state_id: task_params[:task_state_id]
                        
    @task.due_date = @dueDate
    @task.taskboard = @taskboard
    
    @dueDate.task = @task
    @dueDate.save!
                        
    respond_to do |format|
      if @task.save
        format.html { redirect_to taskboards_path, notice: 'Task was successfully created.' }
      else
        format.html { redirect_to taskboards_path, notice: 'Task could not be created.' }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    @dueDate = @task.due_date
    
    d = params[:due_date]
    
    @dueDate.date = "#{d['date(1i)']}-#{d['date(2i)']}-#{d['date(3i)']} #{d['date(4i)']}:#{d['date(5i)']}:00"
    
    @dueDate.save!
    
    taskName = @task.name
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to taskboards_path, notice: "Task '#{taskName}' was successfully updated." }
      else
        format.html { redirect_to taskboards_path, alert: "Task '#{taskName}' could not be updated." }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    taskName = @task.name
    @task.destroy
    
    respond_to do |format|
      format.html { redirect_to taskboards_path, notice: "Task '#{taskName}' was successfully destroyed." }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :description, :task_state_id, :due_date_id, :due_date, :taskboard_id)
    end
end
