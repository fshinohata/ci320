class HomeController < ApplicationController

    # GET /
    def index
        @pageTitle = 'Welcome.'
        @navbarTitle = ''
        @navbarDisableOptions = true
    end
end
