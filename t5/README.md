[Relations]: Relations.png

# Trabalho 5 de Web

Trabalho 5 da disciplina de "Tópicos em Programação de Computadores -- CI320", 2018/1.

## O Que é este Trabalho

Um "Taskboard App" simplificado: todos os usuários tem acesso aos mesmos taskboards, grupos e etc.

Esta é a UML do trabalho (idêntico ao T2):
![Relações do trabalho][Relations]

Algumas coisas são "imutáveis":
- `task_states` são padrões do *seeder*, e não é possível criar/alterar/excluir via aplicação.
    - Os padrões são "A Fazer", "Fazendo" e "Completado".


## Banco de Dados

### Entidades

1. `taskboards`:
  - `name:string`
2. `taskboard_groups`:
  - `name:string`
3. `taskboard_groups_taskboards`:
  - `taskboard_id:integer`
  - `taskboard_group_id:integer`
4. `tasks`:
  - `name:string`
  - `description:string`
  - `due_date_id:integer`
  - `task_state_id:integer`
  - `taskboard_id:integer`
5. `due_dates`:
  - `date:datetime`
  - `task_id:integer`
6. `task_states`:
  - `name:string`
7. `users`:
  - `name:string`
  - `email:string`
  - `encrypted_password:string`
  - [*Outros campos do devise*]
8. `roles`:
  - `name:string`
9. `roles_users`:
  - `role_id:integer`
  - `user_id:integer`

### Relações

1. Uma `taskboard` tem [0..\*] `tasks`.
2. Uma `taskboard` pertence a [0..\*] `taskboard_groups`.
3. Uma `task` pertence a **uma** `taskboard`.
4. Uma `task` tem **um** `due_date`.
5. Uma `task` tem **um** `task_state`.
6. Um `due_date` tem **uma** `task`.

## Usuários

Existem dois tipos de usuário: 'Administrador' e 'Usuário normal'.

Por padrão, o *seeder* gera dois usuários, um com cada role:

```bash
# Administrador
email: super@admin.com
senha: 123456

# Usuário normal
email: normal@user.com
senha: 123456
```

**Não é possível registrar novos usuários nesta aplicação.**

### Permissões

Cada usuário tem as seguintes permissões:

- Um administrador pode:
  - Criar/Visualizar/Alterar/Excluir `Taskboard Groups`
  - Criar/Visualizar/Alterar/Excluir `Taskboards`
  - Criar/Visualizar/Alterar/Excluir `Tasks`
- Um usuário normal pode:
  - Visualizar `Taskboard Groups`
  - Visualizar `Taskboards`
  - Criar/Visualizar/Alterar/Excluir `Tasks`

## Páginas

Existem três páginas na aplicação:

- **Página Inicial:** Uma firulinha. Dá as boas vindas ao usuário.
- **Página de Login:** Página padrão da gema 'devise'. O CSS foi levemente alterado, e as rotas de registro foram desativadas.
- **Página da Aplicação:** Contém o projeto funcional. É separada em duas partes principais:
  - `Taskboard Groups` (parte esquerda): Mostra a lista de grupos disponíveis, além da opção "ALL". Ao clicar em um grupo, apenas as taskboards que estão naquele grupo são exibidas. Para editar um grupo, basta clicar no ícone referente à edição quando estiver logado como administrador. A inserção de taskboards em um grupo ocorre apenas na aba de edição de uma taskboard.
  - `Taskboards` (parte direita): Contém as taskboards em si. Cada taskboard tem suas `tasks`. Para criar uma nova taskboard, basta clicar no botao "New Taskboard" no canto superior esquerdo. Para editar uma taskboard, basta clicar em seu NOME e, no modal que aparecer, clicar no ícone referente à edição.