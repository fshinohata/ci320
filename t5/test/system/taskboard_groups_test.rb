require "application_system_test_case"

class TaskboardGroupsTest < ApplicationSystemTestCase
  setup do
    @taskboard_group = taskboard_groups(:one)
  end

  test "visiting the index" do
    visit taskboard_groups_url
    assert_selector "h1", text: "Taskboard Groups"
  end

  test "creating a Taskboard group" do
    visit taskboard_groups_url
    click_on "New Taskboard Group"

    fill_in "Name", with: @taskboard_group.name
    click_on "Create Taskboard group"

    assert_text "Taskboard group was successfully created"
    click_on "Back"
  end

  test "updating a Taskboard group" do
    visit taskboard_groups_url
    click_on "Edit", match: :first

    fill_in "Name", with: @taskboard_group.name
    click_on "Update Taskboard group"

    assert_text "Taskboard group was successfully updated"
    click_on "Back"
  end

  test "destroying a Taskboard group" do
    visit taskboard_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Taskboard group was successfully destroyed"
  end
end
