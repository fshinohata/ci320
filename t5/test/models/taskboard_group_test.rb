require 'test_helper'

class TaskboardGroupTest < ActiveSupport::TestCase
  setup do
    @taskboardGroup = taskboard_groups(:one)
    @badTaskboardGroup = TaskboardGroup.new name: nil
    @taskboard1 = taskboards(:one)
    @taskboard2 = taskboards(:two)
  end
  
  test "should NOT create taskboard group" do
    assert_not @badTaskboardGroup.save
  end
  
  test "should create taskboard group" do
    @badTaskboardGroup.name = 'Potato Salad'
    assert @badTaskboardGroup.save
  end
  
  test "should insert taskboards" do
    assert @taskboardGroup.taskboards << [@taskboard1, @taskboard2]
    assert @taskboardGroup.save
  end
  
  test "should insert taskboards and then NOT insert duplicates" do
    assert @taskboardGroup.taskboards << [@taskboard1, @taskboard2]
    assert @taskboardGroup.save
    assert_not @taskboardGroup.taskboards << @taskboard1
    assert_not @taskboardGroup.save
  end
end
