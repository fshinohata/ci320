require 'test_helper'

class TaskboardTest < ActiveSupport::TestCase
  setup do
    @taskboard = taskboards(:one)
    @badTaskboard = Taskboard.new name: nil
    @taskboardGroup1 = taskboard_groups(:one)
    @taskboardGroup2 = taskboard_groups(:two)
  end
  
  test "should NOT create taskboard group" do
    assert_not @badTaskboard.save
  end
  
  test "should create taskboard group" do
    @badTaskboard.name = 'Potato Salad'
    assert @badTaskboard.save
  end
  
  test "should insert taskboards" do
    assert @taskboard.taskboard_groups << [@taskboardGroup1, @taskboardGroup2]
    assert @taskboard.save
  end
  
  test "should insert taskboards and then NOT insert duplicates" do
    assert @taskboard.taskboard_groups << [@taskboardGroup1, @taskboardGroup2]
    assert @taskboard.save
    assert_not @taskboard.taskboard_groups << @taskboardGroup1
    assert_not @taskboard.save
  end
end
