require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  setup do
    @role = Role.create name: :admin
    @duplicate = Role.create name: :admin
    @user = User.create name: :administrator, email: 'test@test.com', password: '123456'
  end

  test "should create role" do
    assert @role.save
  end

  test "should NOT create role" do
    @role.save
    assert_not @duplicate.save
  end

  test "should add role to user" do
    assert @role.users << @user 
  end

  test "should NOT add role to user (adding duplicated roles)" do
    @role.users << @user
    assert @role.save
    assert_not @role.users << @user
  end
end
