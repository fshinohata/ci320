require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @role = Role.create name: :admin
    @user = User.create name: :administrator, email: 'test@test.com', password: '123456'
    @duplicate = User.create name: :administrator, email: 'test@test.com', password: '123456'
  end

  test "should create user" do
    assert @user.save
  end

  test "should NOT create user" do
    @user.save
    assert_not @duplicate.save
  end

  test "should add role to user" do
    assert @user.roles << @role 
  end

  test "should NOT add user to user (adding duplicated roles)" do
    @user.roles << @role
    assert @user.save
    assert_not @user.roles << @role
  end
end
