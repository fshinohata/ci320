require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  setup do
    @dueDate = due_dates(:one)
    @taskState = task_states(:one)
    @taskboard = taskboards(:one)
    @task = tasks(:one)
    @notCreatedTask = Task.new name: 'HELLO', description: 'HELLOU', due_date_id: @dueDate.id, task_state_id: @taskState.id, taskboard_id: @taskboard.id
  end
  
  test "should create task" do
    assert @notCreatedTask.save
  end
  
  test "should NOT create task" do
    @notCreatedTask.task_state = nil
    assert_not @notCreatedTask.save
  end
  
  test "should NOT update task" do
    @task.task_state = nil
    assert_not @task.save
  end
end
