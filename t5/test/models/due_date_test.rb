require 'test_helper'

class DueDateTest < ActiveSupport::TestCase
  setup do
    @task = tasks(:one)
    @dueDate = DueDate.new date: '2018-06-13', task_id: @task.id
    @badDueDate = DueDate.new date: '2018-04-09', task_id: 0
  end

  test "should create due date" do
    assert @dueDate.save
  end

  test "should NOT create due date" do
    assert_not @badDueDate.save
  end
end
