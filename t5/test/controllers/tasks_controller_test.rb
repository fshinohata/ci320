require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
    @taskboard = taskboards(:one)
    @taskState = task_states(:two)
    @dueDate = {
      'date(1i)' => '2018',
      'date(2i)' => '06',
      'date(3i)' => '13',
      'date(4i)' => '15',
      'date(5i)' => '00',
    }
    sign_in User.create(email: 'super@admin.com', password: '123456')
  end

  test "should create task" do
    assert_difference('Task.count') do
      post tasks_url, params: { due_date: @dueDate, task: { description: @task.description, name: @task.name, task_state_id: @taskState.id, taskboard_id: @taskboard.id } }
    end

    assert_redirected_to taskboards_url
  end

  test "should update task" do
    patch task_url(@task), params: { due_date: @dueDate, task: { description: @task.description, name: @task.name, task_state_id: @taskState.id, taskboard_id: @taskboard.id } }
    assert_redirected_to taskboards_url
  end

  test "should destroy task" do
    assert_difference('Task.count', -1) do
      delete task_url(@task)
    end

    assert_redirected_to taskboards_url
  end
end
