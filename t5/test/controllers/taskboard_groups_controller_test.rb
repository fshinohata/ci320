require 'test_helper'

class TaskboardGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @taskboard_group = taskboard_groups(:two)
    sign_in User.create(email: 'super@admin.com', password: '123456')
  end

  test "should create taskboard_group" do
    assert_difference('TaskboardGroup.count') do
      post taskboard_groups_url, params: { taskboard_group: { name: @taskboard_group.name } }
    end

    assert_redirected_to taskboards_url
  end

  test "should update taskboard_group" do
    patch taskboard_group_url(@taskboard_group), params: { taskboard_group: { name: 'Potato Salad' } }
    assert_redirected_to taskboards_url
  end

  test "should destroy taskboard_group" do
    assert_difference('TaskboardGroup.count', -1) do
      delete taskboard_group_url(@taskboard_group)
    end

    assert_redirected_to taskboards_url
  end
end
