require 'test_helper'

class TaskboardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @taskboard = taskboards(:one)
    @taskboard_groups = { id: [""] }
    sign_in User.create(email: 'super@admin.com', password: '123456')
  end

  test "should create taskboard" do
    assert_difference('Taskboard.count') do
      post taskboards_url, params: { taskboard: { name: @taskboard.name }, taskboard_group: @taskboard_groups }
    end

    assert_redirected_to taskboards_url
  end

  test "should update taskboard" do
    patch taskboard_url(@taskboard), params: { taskboard: { name: @taskboard.name }, taskboard_group: @taskboard_groups  }
    assert_redirected_to taskboards_url
  end

  test "should destroy taskboard" do
    assert_difference('Taskboard.count', -1) do
      delete taskboard_url(@taskboard)
    end

    assert_redirected_to taskboards_url
  end
end
