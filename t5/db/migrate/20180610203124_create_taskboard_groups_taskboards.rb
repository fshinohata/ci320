class CreateTaskboardGroupsTaskboards < ActiveRecord::Migration[5.2]
  def change
    create_table :taskboard_groups_taskboards do |t|
      t.references :taskboard, foreign_key: true
      t.references :taskboard_group, foreign_key: true

      t.timestamps
    end
  end
end
