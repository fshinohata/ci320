class CreateTaskboards < ActiveRecord::Migration[5.2]
  def change
    create_table :taskboards do |t|
      t.string :name, unique: true, null: false, default: ""
      
      t.timestamps
    end
  end
end
