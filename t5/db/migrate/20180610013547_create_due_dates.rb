class CreateDueDates < ActiveRecord::Migration[5.2]
  def change
    create_table :due_dates do |t|
      t.datetime :date, null: false
      t.references :task, foreign_key: true

      t.timestamps
    end
  end
end
