class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.string :description
      t.references :task_state, foreign_key: true
      t.references :due_date, foreign_key: true
      t.references :taskboard, foreign_key: true

      t.timestamps
    end
  end
end
