# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ------------------------------------ #
# USERS
# ------------------------------------ #

u1 = User.where(email: 'super@admin.com').first_or_create do |u|
    u.name = "Administrator"
    u.email = 'super@admin.com'
    u.password =  '123456'
    u.password_confirmation =  '123456'
end

u2 = User.where(email: 'normal@user.com').first_or_create do |u|
    u.name = "NormalUser"
    u.email = 'normal@user.com'
    u.password =  '123456'
    u.password_confirmation =  '123456'
end



# ------------------------------------ #
# ROLES
# ------------------------------------ #

r1 = Role.where(name: 'administrator').first_or_create do |r|
    r.name = 'administrator'
end

r2 = Role.where(name: 'standard_user').first_or_create do |r|
    r.name = 'standard_user'
end



# ------------------------------------ #
# TASKBOARDS
# ------------------------------------ #

TB1 = Taskboard.create name: 'CI320 - Web'
TB2 = Taskboard.create name: 'CI061 - Redes II'
TB3 = Taskboard.create name: 'CI065 - Grafos'
TB4 = Taskboard.create name: 'Ecomp'
TB5 = Taskboard.create name: 'Estágio'
TB6 = Taskboard.create name: 'Compras'



# ------------------------------------ #
# TASKBOARD GROUPS
# ------------------------------------ #

TBG1 = TaskboardGroup.create name: 'BCC'
TBG2 = TaskboardGroup.create name: 'UFPR'
TBG3 = TaskboardGroup.create name: 'Pessoal'



# ------------------------------------ #
# TASK STATES
# ------------------------------------ #

TS_DONE = TaskState.create name: 'Completado'
TS_TODO = TaskState.create name: 'A Fazer'
TS_DOING = TaskState.create name: 'Fazendo'

# ------------------------------------ #
# TASKS AND DUE DATES
# ------------------------------------ #

task1 = Task.create name: 'Trabalho 5', description: 'Último trabalho da disciplina'
D1 = DueDate.create date: '2018-06-11'

task2 = Task.create name: 'Trabalho Prático', description: 'Servidor-Calculadora distribuído'
D2 = DueDate.create date: '2018-06-08'

task3 = Task.create name: 'Trabalho 1', description: 'Calculador de grafo de recomendações de grafos bipartidos produto-consumidor'
D3 = DueDate.create date: '2018-05-10'

task4 = Task.create name: 'Trabalho 2', description: 'Coloração ótima de grafos cordais e distância-hereditários'
D4 = DueDate.create date: '2018-06-23'

task5 = Task.create name: 'Aprender ReactJS, Redux, KnockoutJS e .NET core', description: 'Aprender o quanto antes'
D5 = DueDate.create date: '2018-06-30'

task6 = Task.create name: 'Terminar o Midas', description: 'Terminar o Midas'
D6 = DueDate.create date: '2018-06-08'

task7 = Task.create name: 'Comprar chocolate', description: 'Mais açúcar é necessário'
D7 = DueDate.create date: '2018-06-10'



# ------------------------------------ #
# RELATIONSHIPS
# ------------------------------------ #


r1.users << u1
r2.users << u2

TBG1.taskboards << [TB1, TB2, TB3]
TBG2.taskboards << [TB1, TB2, TB3, TB4, TB5]
TBG3.taskboards << TB6

task1.taskboard = TB1
task1.task_state = TS_DOING
task1.due_date = D1

task2.taskboard = TB2
task2.task_state = TS_DONE
task2.due_date = D2

task3.taskboard = TB3
task3.task_state = TS_DONE
task3.due_date = D3

task4.taskboard = TB3
task4.task_state = TS_DOING
task4.due_date = D4

task5.taskboard = TB5
task5.task_state = TS_DOING
task5.due_date = D5

task6.taskboard = TB4
task6.task_state = TS_DOING
task6.due_date = D6

task7.taskboard = TB6
task7.task_state = TS_TODO
task7.due_date = D7

D1.task = task1
D2.task = task2
D3.task = task3
D4.task = task4
D5.task = task5
D6.task = task6
D7.task = task7

# ------------------------------------ #
# REAL SAVE TO DATABASE
# ------------------------------------ #

u1.save!
u2.save!
r1.save!
r2.save!

TBG1.save!
TBG2.save!
TBG3.save!

TB1.save!
TB2.save!
TB3.save!
TB4.save!
TB5.save!
TB6.save!

task1.save!
task2.save!
task3.save!
task4.save!
task5.save!
task6.save!
task7.save!

D1.save!
D2.save!
D3.save!
D4.save!
D5.save!
D6.save!
D7.save!

TS_DONE.save!
TS_DOING.save!
TS_TODO.save!

