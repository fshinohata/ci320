##################################################
##
##  SOME NOTES:
##    - The "File.dirname(__FILE__)" returns the absolute path to the script. 
##      It's necessary if you run schema.rb indirectly, from another script.
##
##    - Do NOT rename any of the subfolders contained within this folder.
##      Their names are hard coded within this file!
##
##################################################
require_relative 'connect'

DatabaseSeederPath = 'seeds/DatabaseSeeder'
MigrationsFolder = 'migrations'

def create_tables
	migrations = Dir[File.dirname(__FILE__) + "/#{MigrationsFolder}/*.rb"]

	for migration in migrations
		pid = Process.fork do
			require_relative migration
			up
			Process.exit
		end
		Process.wait pid
	end
end

def drop_tables
	migrations = Dir[File.dirname(__FILE__) + "/#{MigrationsFolder}/*.rb"]

	for migration in migrations
		pid = Process.fork do
			require_relative migration
			down
			Process.exit
		end
		Process.wait pid
	end
end

def database_seeder
	pid = Process.fork do
		require_relative DatabaseSeederPath
		run
		Process.exit
	end
	Process.wait pid
end

case ARGV[0]
	when '', 'help', 'list'
		print "Schema Options:\n"
		print "   help | list                -- Display the options and exit\n"
		print "   schema:up                  -- Creates all tables\n"
		print "   schema:down                -- Drops all tables\n"
		print "   schema:refresh [options]   -- Drops and creates all tables\n"
		print "      Available options:\n"
		print "         --seed : Runs the database seeder after refreshing\n"
		print "   schema:seed                -- Runs the database seeder\n"
	when 'schema:up'
		print "CREATING TABLES\n"
		create_tables
	when 'schema:down'
		print "DROPPING TABLES\n"
		$schema.execute("PRAGMA foreign_key_checks = OFF;")
		drop_tables
		$schema.execute("PRAGMA foreign_key_checks = ON;")
	when 'schema:refresh'
		print "REFRESHING DATABASE\n"
		drop_tables
		create_tables
		if ARGV[1] == '--seed'
			print "\nSEEDING DATABASE\n"
			database_seeder
		end
	when 'schema:seed'
		database_seeder
	else
		print "Unknown command '#{command}'.\nType 'help' to show available commands\n"
end
