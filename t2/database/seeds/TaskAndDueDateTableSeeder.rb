require_relative '../models/Task'
require_relative '../models/DueDate'
require_relative '../models/TaskState'
require_relative '../models/Taskboard'

def getTaskboard code
	taskboard = Taskboard.where('name LIKE ?', "%#{code}%").first!
	
	if taskboard == nil
		print "[TaskTableSeeder] ERROR: No taskboard with '#{code}' was found in database!"
		exit 0
	end

	return taskboard
end

def getTaskState code
	taskboard = TaskState.where('name LIKE ?', "%#{code}%").first!
	
	if taskboard == nil
		print "[TaskTableSeeder] ERROR: No task state with '#{code}' was found in database!"
		exit 0
	end

	return taskboard
end

def run
	print "Seeding 'tasks' and 'due_dates' table...\n"
	state_todo = getTaskState 'A Fazer'
	state_done = getTaskState 'Encerrada'

	#----------------------------------------------------
	# CI320
	taskboard = getTaskboard 'CI320'
	due_date = DueDate.new(:date => "2018-03-26 23:59:59")

	t = Task.new(:name => "Trabalho 1",
				 :description => "Cria própria página do departamento",
				 :task_state_id => state_done.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-03-09 23:59:59")
	t = Task.new(:name => "Trabalho 2",
				 :description => "Aplicação de Banco de Dados em Active Record",
				 :task_state_id => state_done.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-04-30 23:59:59")
	t = Task.new(:name => "Trabalho 3",
				 :description => "Processador de retas e polígonos",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-05-14 23:59:59")
	t = Task.new(:name => "Trabalho 4",
				 :description => "Visualizeitor",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-06-11 23:59:59")
	t = Task.new(:name => "Trabalho 5",
				 :description => "Aplicação Rails",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save
	

	#----------------------------------------------------
	# CI065
	taskboard = getTaskboard 'CI065'

	due_date = DueDate.new(:date => "2018-04-22 23:59:59")
	t = Task.new(:name => "Trabalho 1",
				 :description => "Fazer um sistema de recomendações rudimentar baseado em grafos ponderados",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-04-11 15:30:00")
	t = Task.new(:name => "Estudar Para P1",
				 :description => "Exercícios de 1 até 72",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save	
	due_date.task_id = t.id
	due_date.save



	#----------------------------------------------------
	# CI061
	taskboard = getTaskboard 'CI061'

	due_date = DueDate.new(:date => "2018-04-12 19:30:00")
	t = Task.new(:name => "Estudar Para P1",
				 :description => "Exercícios das listas na página",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save



	#----------------------------------------------------
	# Ecomp
	taskboard = getTaskboard 'Ecomp'

	due_date = DueDate.new(:date => "2018-04-11 23:59:59")
	t = Task.new(:name => "Curso de JavaScript",
				 :description => "Montar o curso de JavaScript para membros novos",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-04-13 23:59:59")
	t = Task.new(:name => "Curso de CSS/SASS com Laravel Mix",
				 :description => "Curso para membros antigos, focado em Front End",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-04-15 23:59:59")
	t = Task.new(:name => "Curso de JQuery e Ajax com Laravel",
				 :description => "Curso para membros antigos, englobando o básico de JQuery e focado no funcionamento de Ajax com Laravel",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save



	#----------------------------------------------------
	# PET
	taskboard = getTaskboard 'PET'

	due_date = DueDate.new(:date => "2018-12-31 23:59:59")
	t = Task.new(:name => "Terminar o Farol",
				 :description => "Desta vez o farol deve ficar pronto!",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save

	due_date = DueDate.new(:date => "2018-12-31 23:59:59")
	t = Task.new(:name => "Terminar o A.D.E.G.A.",
				 :description => "Desta vez o A.D.E.G.A. deve ficar pronto!",
				 :due_date => due_date.id)
	t.task_state_id = state_todo.id
	t.taskboard_id = taskboard.id
	t.save
	due_date.task_id = t.id
	due_date.save



	#----------------------------------------------------
	# Compras
	taskboard = getTaskboard 'Compras'

	due_date = DueDate.new(:date => "2018-04-01 23:59:59")
	t = Task.new(:name => "Caixa de Chocolate",
				 :description => "A última acabou. Preciso de mais chocolate.",
				 :task_state_id => state_todo.id,
				 :due_date => due_date.id,
				 :taskboard_id => taskboard.id)
	t.save
	due_date.task_id = t.id
	due_date.save
end
