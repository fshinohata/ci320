require_relative '../models/Taskboard'
require_relative '../models/TaskboardGroup'

def getTaskboard code
	taskboard = Taskboard.where('name LIKE ?', "%#{code}%").first!
	
	if taskboard == nil
		print "[TaskboadGroupTableSeeder] ERROR: No taskboard with '#{code}' was found in database!"
		exit 0
	end

	return taskboard
end

def run
	print "Seeding 'taskboard_groups' table...\n"
	
	t = TaskboardGroup.new(:name => 'UFPR')
	t.taskboards << getTaskboard('CI320')
	t.taskboards << getTaskboard('CI065')
	t.taskboards << getTaskboard('CI061')
	t.taskboards << getTaskboard('Ecomp')
	t.taskboards << getTaskboard('PET')
	t.save

	t = TaskboardGroup.new(:name => 'BCC')
	t.taskboards << getTaskboard('CI320')
	t.taskboards << getTaskboard('CI065')
	t.taskboards << getTaskboard('CI061')
	t.save

	t = TaskboardGroup.new(:name => 'Extra Curriculares')
	t.taskboards << getTaskboard('Ecomp')
	t.taskboards << getTaskboard('PET')
	t.save

	t = TaskboardGroup.new(:name => 'Pessoal')
	t.taskboards << getTaskboard('Compras')
	t.save
end