require_relative '../models/TaskState'

def run
	print "Seeding 'task_states' table...\n"
	
	t = TaskState.new(:name => 'Encerrada')
	t.save

	t = TaskState.new(:name => 'A Fazer')
	t.save

	t = TaskState.new(:name => 'On Hold')
	t.save
end