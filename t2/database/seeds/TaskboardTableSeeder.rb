require_relative '../models/Taskboard'

def run
	print "Seeding 'taskboards' table...\n"

	t = Taskboard.new(:name => 'CI320 - Web')
	t.save

	t = Taskboard.new(:name => 'CI065 - Grafos')
	t.save

	t = Taskboard.new(:name => 'CI061 - Redes II')
	t.save

	t = Taskboard.new(:name => 'Ecomp')
	t.save

	t = Taskboard.new(:name => 'PET')
	t.save

	t = Taskboard.new(:name => 'Compras')
	t.save
end