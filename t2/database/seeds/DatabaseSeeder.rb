def run
	# Place the name of the seeders in run order
	seeders = [
		'TaskboardTableSeeder',
		'TaskboardGroupTableSeeder',
		'TaskStateTableSeeder',
		'TaskAndDueDateTableSeeder'
	]

	for seeder in seeders
		pid = Process.fork do
			require_relative seeder
			run
			Process.exit
		end
		Process.wait pid
	end
end
