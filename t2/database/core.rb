require 'rubygems'
require 'active_record'
require_relative 'connect'

def load_models
	standard_classes = Object.constants

	for model in Dir[File.dirname(__FILE__) + "/models/*.rb"]
		require_relative model
	end

	model_classes = Object.constants - standard_classes
	return model_classes
end

def boot model_classes
	# if at least one table from the models doesn't exist, 
	# the applications refreshes the database (e.g. drop & create all tables)
	# and asks the user if he wants to seed the database
	for cls in model_classes
		if !$schema.table_exists? Object.const_get(cls).table
			print "Database is not up to date. Refreshing...\n"

			pid = Process.fork do
				ARGV[0] = 'schema:refresh'
				require_relative 'schema'
				Process.exit
			end
			Process.wait pid

			# Reloads everything after creating new tables
			Object.class_eval do
				for model in model_classes
					remove_const model
				end
			end

			ActiveRecord::Base.clear_active_connections!
			load File.dirname(__FILE__) + '/connect.rb'

			for model in Dir[File.dirname(__FILE__) + "/models/*.rb"]
				load model
			end

			print "Want to run the database seeder? (y/n) "

			loop do
				answer = $stdin.getc
				if answer == 'y'
					pid = Process.fork do
						ARGV[0] = 'schema:seed'
						require_relative 'schema'
						Process.exit
					end
					Process.wait pid

					$stdin.gets
					break
				end
			end
			break
		end
	end
end

def search_table models, table
	for model in models
		if Object.const_get(model).table == table
			return model
		end
	end

	return nil
end

def insert model, data
	query_array = []
	for query in data
		if !query.include? '='
			puts "Syntax error: No value given to attribute '#{query}'."
			return -1
		end
		query.gsub!(/["]/, "")
		query = query.split(/=/)

		if !Object.const_get(model).column_names.include? query[0]
			puts "Error: invalid attribute '#{query[0]}' for table '#{Object.const_get(model).table}'"
			return -1
		end

		query_array << query
	end

	query_hash = Hash[query_array]

	m = Object.const_get(model).new(query_hash)
	m.save

	puts "Entry '#{m.name}' created successfully!"

	return 0
end

def show_tables models
	for model in models
		obj = Object.const_get(model)
		puts obj.table + " (#{obj.all.count} rows)"
	end
end

def show model, filter
	if filter
		filter = ['id', 'created_at', 'updated_at']
	else
		filter = []
	end

	Object.const_get(model).columns.each do |column|
		if !filter.include? column.name
			print "|  #{column.name}  "
		end
	end
	puts "|"

	for obj in Object.const_get(model).all
		obj.attributes.each_pair do |key,value|
			if !filter.include? key
				print "|  #{value}  "
			end
		end
		puts "|"
	end
end

def destroy model, data
	query_array = []

	if data.length == 0
		Object.const_get(model).destroy_all
		return 0
	end

	for query in data
		if !query.include? '='
			puts "Syntax error: No value given to attribute '#{query}'."
			return -1
		end
		query.gsub!(/["]/, "")
		query = query.split(/=/)
		query_array << query

		if !Object.const_get(model).column_names.include? query[0]
			puts "Error: invalid attribute '#{query[0]}' for table '#{Object.const_get(model).query}'"
			return -1
		end
	end

	query_hash = Hash[query_array]
		
	Object.const_get(model).where(query_hash).destroy_all

	return 0
end

def update_by_id model, data, id
	search_hash = {:id => id}
	update_array = []

	for query in data
		if !query.include? '='
			puts "Syntax error: No value given to attribute '#{query}'."
			return -1
		end
		query.gsub!(/["]/, "")
		query = query.split(/=/)

		if !Object.const_get(model).column_names.include? query[0]
			puts "Error: invalid attribute '#{query[0]}' for table '#{Object.const_get(model).query}'"
			return -1
		end

		update_array << query
	end

	update_hash = Hash[update_array]

	Object.const_get(model).where(search_hash).find_each { |m| 
		m.update_attributes(update_hash)
	}

	return 0
end

def update model, data
	search_array = []
	update_array = []

	if data.length == 0
		puts "ERROR: command 'altera' needs at least one attribute to search!"
		return -1
	end

	for query in data
		if query[/^id=/] == 'id='
			query = query.split(/=/)
			return update_by_id model, data, query[1].to_i
		end
	end

	for query in data
		if !query.include? '='
			puts "Syntax error: No value given to attribute '#{query}'."
			return -1
		end
		query.gsub!(/["]/, "")
		query = query.split(/=/)

		found = false
		search_array.each { |item| 
			if item.include? query[0]
				found = true
			end
		}

		if !Object.const_get(model).column_names.include? query[0]
			puts "Error: invalid attribute '#{query[0]}' for table '#{Object.const_get(model).query}'"
			return -1
		end

		if !found
			search_array << query
		else
			update_array << query
		end
	end

	search_hash = Hash[search_array]
	update_hash = Hash[update_array]

	Object.const_get(model).where(search_hash).find_each { |m| 
		m.update_attributes(update_hash)
	}

	return 0
end
