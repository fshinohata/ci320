require 'rubygems'
require 'active_record'
require_relative '../connect'

def up
	if !$schema.table_exists? 'task_states'
		$schema.create_table :task_states do |t|
			t.string   :name
			t.timestamps null: true
		end
		print "Table 'task_states' created...\n"
	else
		print "Table 'task_states' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'task_states'
		$schema.drop_table :task_states
		print "Table 'task_states' dropped...\n"
	else
		print "Table 'task_states' doesn't exist!\n"
	end
end
