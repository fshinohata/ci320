require 'rubygems'
require 'active_record'

def up
	if !$schema.table_exists? 'taskboards'
		$schema.create_table :taskboards do |t|
			t.string  :name
			t.timestamps(null: true)
		end
		print "Table 'taskboards' created...\n"
	else
		print "Table 'taskboards' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'taskboards'
		$schema.drop_table :taskboards
		print "Table 'taskboards' dropped...\n"
	else
		print "Table 'taskboards' doesn't exist!\n"
	end
end
