require 'rubygems'
require 'active_record'
require_relative '../connect'

def up
	if !$schema.table_exists? 'tasks'
		$schema.create_table :tasks do |t|
			t.string      :name
			t.string      :description
			t.references  :task_state, foreign_key: true
			t.references  :due_date, foreign_key: true
			t.references  :taskboard, foreign_key: true
			t.timestamps(null: true)
		end
		print "Table 'tasks' created...\n"
	else
		print "Table 'tasks' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'tasks'
		$schema.drop_table :tasks
		print "Table 'tasks' dropped...\n"
	else
		print "Table 'tasks' doesn't exist!\n"
	end
end
