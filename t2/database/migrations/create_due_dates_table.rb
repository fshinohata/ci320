require 'rubygems'
require 'active_record'
require_relative '../connect'

def up
	if !$schema.table_exists? 'due_dates'
		$schema.create_table :due_dates do |t|
			t.datetime   :date
			t.references :task, foreign_key: true
			t.timestamps null: true
		end
		print "Table 'due_dates' created...\n"
	else
		print "Table 'due_dates' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'due_dates'
		$schema.drop_table :due_dates
		print "Table 'due_dates' dropped...\n"
	else
		print "Table 'due_dates' doesn't exist!\n"
	end
end
