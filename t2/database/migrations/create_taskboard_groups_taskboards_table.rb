require 'rubygems'
require 'active_record'
require_relative '../connect'

def up
	if !$schema.table_exists? 'taskboard_groups_taskboards'
		$schema.create_table :taskboard_groups_taskboards do |t|
			t.references :taskboard, foreign_key: true
			t.references :taskboard_group, foreign_key: true
			t.timestamps(null: true)
		end

		print "Table 'taskboard_groups_taskboards' created...\n"
	else
		print "Table 'taskboard_groups_taskboards' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'taskboard_groups_taskboards'
		$schema.drop_table :taskboard_groups_taskboards
		print "Table 'taskboard_groups_taskboards' dropped...\n"
	else
		print "Table 'taskboard_groups_taskboards' doesn't exist!\n"
	end
end
