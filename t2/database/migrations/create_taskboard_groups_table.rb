require 'rubygems'
require 'active_record'
require_relative '../connect'

def up
	if !$schema.table_exists? 'taskboard_groups'
		$schema.create_table :taskboard_groups do |t|
			t.string  :name
			t.timestamps(null: true)
		end
		print "Table 'taskboard_groups' created...\n"
	else
		print "Table 'taskboard_groups' already exists!\n"
	end
end

def down
	if $schema.table_exists? 'taskboard_groups'
		$schema.drop_table :taskboard_groups
		print "Table 'taskboard_groups' dropped...\n"
	else
		print "Table 'taskboard_groups' doesn't exist!\n"
	end
end
