##################################################
##
##  SOME NOTES:
##    - The File.dirname(__FILE__) return the absolute path to the script. 
##      It's necessary if you run connect.rb indirectly, from another script.
##
##################################################

require 'rubygems'
require 'active_record'

ActiveRecord::Base.establish_connection :adapter => "sqlite3", :database => File.dirname(__FILE__) + "/Database.sqlite3"

$schema = ActiveRecord::Base.connection
