require 'active_record'

class TaskState < ActiveRecord::Base
	def self.table
		return 'task_states'
	end

	def update_attributes params
		@old_name = name
		super params
	end

	after_update { |taskboard| puts "Task State '#{@old_name}' updated to '#{name}' successfully!" }

	after_destroy { |task_state| 
		print "Query OK. Entry '#{name}' deleted successfully.\n"
	}
	
	has_many :tasks, dependent: :nullify
end