require 'active_record'

class Task < ActiveRecord::Base
	def self.table
		return 'tasks'
	end

	def update_attributes params
		@old_name = name
		super params
	end

	after_update { |task| puts "Task '#{@old_name}' updated to '#{name}' successfully!" }
	
	after_destroy { |task| 
		print "Query OK. Entry '#{name}' deleted successfully.\n"
	}

	belongs_to :taskboard
	belongs_to :task_state
	has_one    :due_date, dependent: :destroy
end
