require 'active_record'

class DueDate < ActiveRecord::Base
	def self.table
		return 'due_dates'
	end

	def update_attributes params
		@old_date = date
		super params
	end

	after_update { |due_date| puts "Due date '#{@old_date}' updated to '#{date}' successfully!" }
	
	after_destroy { |due_date| 
		print "Query OK. Entry '#{date}' deleted successfully.\n"
	}

	belongs_to :task
end