require 'active_record'
require_relative 'Taskboard'

class TaskboardGroup < ActiveRecord::Base
	def self.table
		return 'taskboard_groups'
	end

	@taskboards_affected

	def destroy
		@taskboards_affected = taskboards.count
		
		# Runs the parent delete procedure
		super
	end

	def update_attributes params
		@old_name = name
		super params
	end

	after_update { |taskboard_group| puts "Taskboard Group '#{@old_name}' updated to '#{name}' successfully!" }

	after_destroy { |taskboard_group|
		print "Query OK. Entry '#{name}' deleted successfully.\n"
		print "#{@taskboards_affected} rows affected in 'taskboard_groups_taskboards'.\n"
	}
	
	has_and_belongs_to_many :taskboards, through: :taskboard_groups_taskboards
end