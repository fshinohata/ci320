require 'active_record'
require_relative 'Task'
require_relative 'TaskboardGroup'

class Taskboard < ActiveRecord::Base
	def self.table
		return 'taskboards'
	end

	def destroy
		@tasks_affected = tasks.count
		@taskboard_groups_affected = taskboard_groups.count

		# Runs the parent destroy procedure
		super
	end

	def update_attributes params
		@old_name = name
		super params
	end

	after_update { |taskboard| puts "Taskboard '#{@old_name}' updated to '#{name}' successfully!" }

	after_destroy { |taskboard|
		print "Query OK. Entry '#{name}' deleted successfully.\n"
		print "#{@tasks_affected} rows affected in '#{Task.table}'.\n"
		print "#{@taskboard_groups_affected} rows affected in 'taskboard_groups_taskboards'.\n"
	}

	has_many :tasks, dependent: :destroy
	has_and_belongs_to_many :taskboard_groups, through: :taskboard_groups_taskboards, dependent: :destroy
end