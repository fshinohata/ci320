# CI320 - Tópicos em Programação de Computadores

Trabalho 2 da disciplina de *web*, do primeiro semestre de 2018.

## Autor

Fernando Aoyagui Shinohata -- GRR20165388

## Estrutura do Trabalho

Este trabalho foi "montado" como uma estrutura simplificada de *frameworks* baseadas em MVC, como *Laravel* e *Ruby on Rails*. O aplicativo de manuseio do banco de dados `app.rb` encontra na raíz. Para executá-lo, basta chamar o interpretador:

```bash
you$ ruby app.rb
```

Abaixo estão descritas as funções de cada arquivo e pasta.

  - `README.html`: Versão em HTML deste documento.
  - `commands_sample.dat`: Arquivo-texto com comandos-exemplo para utilizar em `app.rb`.
  - `Relations.png`: Imagem com uma representação das relações entre as tabelas-padrão desta aplicação.
  - `database/`: Pasta onde ficam todos os arquivos da aplicação.
  - `database/migrations/`: Pasta onde ficam os arquivos de migração (criação/deleção de tabelas) da aplicação.
  - `database/models/`: Pasta onde ficam os arquivos contendo as classes dos `Models` das tabelas do banco de dados.
  - `database/seeds/`: Pasta onde ficam os *seeders* das tabelas.
  - `database/connect.rb`: Script responsável por estabelecer a conexão com o banco de dados.
  - `database/core.rb`: Script contendo as funções utilizadas pela aplicação `app.rb`.
  - `database/schema.rb`: Script independente, que implementa funções capazes de lidar com a criação/exclusão/preenchimento das tabelas do banco de dados.
    - Não é utilizado pela aplicação `app.rb`, com exceção do caso onde o banco de dados não existe ou não condiz com os `Models` registrados (e.g. quando você adiciona um `Model` diferente).
    - Você pode utilizar este script independentemente da aplicação `app.rb`, bastando executar o interpretador sobre ele.


## Como adicionar/alterar tabelas

Para criar novas relações de tabelas, basta criar os arquivos necessários dentro da pasta `database/`:
  
  - As *migrations* para criar as tabelas na pasta `migrations/`.
  - Os *Models* para manusear os dados na pasta `models/`.
  - Opcionalmente, um *seeder* na pasta `seeds/`. Neste caso, após criar o arquivo do *seeder*, você precisará também registrá-lo no arquivo `seeds/DatabaseSeeder.rb`.

Após criar os arquivos, basta reiniciar a aplicação `app.rb` com o comando acima e ele detectará as mudanças automagicamente e executará a rotina de reconstrução do banco de dados (sim, tudo será perdido).
