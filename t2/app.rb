require_relative 'database/connect'
require_relative 'database/core'

print "INITIALIZING...\n"

# load_models is a function from core.rb
model_classes = load_models

# boot is a function from core.rb
boot model_classes

# WELCOME MESSAGES
puts "\nWelcome to the Taskboard Manager app!"
puts "Type 'help' to see which commands are available."

# APP
exit_app = false

while !exit_app
	print "app$ "
	line = $stdin.gets

	# Filtering excessive spaces
	line.strip!
	line.squeeze!(" ")

	# Regex from Siva Kranthi Kumar
	# https://stackoverflow.com/questions/9577930/regular-expression-to-select-all-whitespace-that-isnt-in-quotes
	data = line.split(/\s+(?=(?:[^\'"]*[\'"][^\'"]*[\'"])*[^\'"]*$)/)
	
	case data[0]
		when "insere"
			model = search_table(model_classes, data[1])
			if model == nil
				puts "Table '#{data[1]}' doesn't exist!"
			else
				insert model, data[2..data.size]
			end

		when "altera"
			model = search_table(model_classes, data[1])
			if model == nil
				puts "Table '#{data[1]}' doesn't exist!"
			else
				update model, data[2..data.size]
			end

		when "exclui"
			model = search_table(model_classes, data[1])
			if model == nil
				puts "Table '#{data[1]}' doesn't exist!"
			else
				destroy model, data[2..data.size]
			end

		when "lista"
			if data[1] == nil
				show_tables model_classes
			else
				model = search_table(model_classes, data[1])
				if model == nil
					puts "Table '#{data[1]}' doesn't exist!"
				else
					if data[2] == '--all'
						show model, false
					else
						show model, true
					end
				end
			end

		when "help"
			puts "Available commands:"
			puts "   q|quit|exit -- Exits the application."
			puts "   insere <table> [attr1=val1 [attr2=val2 [...]]] -- Inserts a new entry in <table> with the given attributes. Attributes not specified defaults to nil."
			puts "   exclui <table> [attr1=val1 [attr2=val2 [...]]] -- Excludes from <table> all entries that match the given attributes. If no attributes are specified, every entry in the table will be destroyed."
			puts "   altera <table> [attr1=val1 attr2=val2 new_attr1=new_val1 [...]] -- Changes entries that match the first occurrence of each given attribute with the new values given in the second occurrence of the same attribute. NOTE: if the attribute 'id' is given, every attribute besides it will be considered as an update parameter."
			puts "   lista [<table> [--all]] -- Lists all entries for <table>. If nothing is specified, it will list the name of all tables."
			puts "      --all : Do not filter columns 'id', 'created_at' and 'updated_at'"

		when "q", "quit", "exit"
			exit_app = true

		else
			puts "Unknown command '#{data[0]}'."
			puts "Type 'help' to see which commands are available."
	end

end

print "BYE\n"
